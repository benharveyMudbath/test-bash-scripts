# Test bash scripts

This repo is aimed to be a proof of concept to show that bash scripts can be used to modify html files ready to be pushed to Salesforce E-Commerce platform

## 01-strip-html-tags.sh

Is used to remove the `<body>`, `<head>` and `<html>` tags from an existing html document. These tags should be updated to a `<div>` tag

## 02-update-response-body.sh

Takes an existing JSON object that is used to make request to Salesforce and update the body element to have the parsed html in it

## 03-curl-request.sh

Fetches the bearer token using curl. Then makes a request using the newly created JSON object containing the new html content

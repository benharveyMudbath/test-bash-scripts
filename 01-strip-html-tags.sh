#!/usr/bin/env bash
set -euo pipefail

trap 'catch $? $LINENO' EXIT

catch() {
  if [ "$1" != "0" ]; then
    # error handling goes here
    echo "======================="
    echo "Failed to parse html file"
    echo "Error $1 occurred on $2"
    exit 1
  fi
}

run() {
    # Do a number of things to the target html file
    # 1. Drop the first line of the file. This should be  <!doctype html> and it is not required: -e '1d'
    # 2. Change <html> tags to <div> tags: -e 's/html/div/'
    # 3. Change <head> tags to <div> tags: -e 's/head/div/'
    # 4. Change <body> tags to <div> tags: -e 's/body/div/'
    sed -e '1d' -e 's/html/div/' -e 's/head/div/' -e 's/body/div/' test.html > output.html

    # 5. Remove all the newline characters from the file
    awk 1 ORS=' ' output.html >> output-newline-stripped.html

    echo "Successfully parsed html"
    cat output-newline-stripped.html
}

run

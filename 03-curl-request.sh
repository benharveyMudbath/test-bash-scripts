#!/usr/bin/env bash
set -euo pipefail

trap 'catch $? $LINENO' EXIT

catch() {
  if [ "$1" != "0" ]; then
    # error handling goes here
    echo "======================="
    echo "Failed to update api with new html"
    echo "Error $1 occurred on $2"
    exit 1
  fi
}

run() {
    # Fetch auth token and save as a variable
   ACCESSTOKEN=$(curl --location --request POST 'https://account.demandware.com/dwsso/oauth2/access_token?=' \
      --header 'Authorization: Basic MjYwNjQzMjctYmRhNC00MTYyLTgzNjctYTU3ZmQ4MDhiM2Y4OlB0RUM0OXAjSiNHcA==' \
      --header 'Content-Type: application/x-www-form-urlencoded' \
      --header 'Cookie: route=1600903478.753.142.431776' \
      --data-urlencode 'grant_type=client_credentials' \
      | jq .access_token)

    T=$(echo $ACCESSTOKEN | sed -e 's/"//g') # remove the " characters from the variables

    # Make http request to update the content of the site
    curl -v -f --request PUT \
      --url https://dev10-web-michaelhill.demandware.net/s/-/dw/data/v20_9/libraries/MichaelHillAU/content/nextjs-from-bee \
      --header "authorization: Bearer ${T}" \
      --header 'content-type: application/json' \
      --header 'x-dw-client-id: 26064327-bda4-4162-8367-a57fd808b3f8' \
      -d ./output.json

    echo "Successfully updated api with new html"
}

run

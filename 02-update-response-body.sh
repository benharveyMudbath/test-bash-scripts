#!/usr/bin/env bash
set -euo pipefail

trap 'catch $? $LINENO' EXIT

catch() {
  if [ "$1" != "0" ]; then
    # error handling goes here
    echo "======================="
    echo "Failed to update response body"
    echo "Error $1 occurred on $2"
    exit 1
  fi
}

run() {
    # Take the template json body and replace with the required html string
    sed "s|replaceme|$(cat output-newline-stripped.html)|" sample-json-body.json > output.json

    echo "Succssfully update json request object"
    cat output.json
}

run
